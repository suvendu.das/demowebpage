package webapp.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonFunctions {
	
	private WebDriver driver;
	
	public CommonFunctions(WebDriver driver){
		this.driver = driver;
	}
	
	public void launchEnv(WebDriver driver){
		driver.get("http://localhost:8081/DemoWebPage/");
	}

	public String getWebPageTitle() {
		return(driver.getTitle());
		
	}

	public WebElement getWebElement(String webBody) {
		return driver.findElement(By.xpath(webBody));
	}

	public String getWebCssValue(WebElement webelement, String value){
		return webelement.getCssValue(value);
	}

	public void endtest(WebDriver driver) {
		driver.quit();		
	}
	
}
