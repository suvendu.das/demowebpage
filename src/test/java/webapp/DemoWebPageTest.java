package webapp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import junit.framework.Assert;
import webapp.DemoWebPage.DemoWebPage;
import webapp.framework.CommonFunctions;

public class DemoWebPageTest {

	@Test
	public void demowebpagetest() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		CommonFunctions commonfunctions = new CommonFunctions(driver);
		commonfunctions.launchEnv(driver);
		DemoWebPage demowebpage = new DemoWebPage(driver);
		Assert.assertTrue(demowebpage.verifyTitle());
		Assert.assertTrue(demowebpage.verifyPageColor());
		commonfunctions.endtest(driver);
	}

}
