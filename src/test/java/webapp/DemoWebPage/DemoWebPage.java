package webapp.DemoWebPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import webapp.framework.CommonFunctions;

public class DemoWebPage {
	private WebDriver driver;
	CommonFunctions commonfunction;
	
	private String webBody = "//body";
	private String expectedColor = "#ffa500";
	
	public DemoWebPage(WebDriver driver){
		this.driver = driver;
		commonfunction = new CommonFunctions(this.driver);
	}
	
	public boolean verifyTitle(){
		String webPageTitle = commonfunction.getWebPageTitle();
		if(!webPageTitle.trim().equalsIgnoreCase("Test Page")){
			System.out.println("Page title is incorrect");
			return false;
		}
		else{
			System.out.println("Page title is correct");
			return true;
		}
	}
	
	public boolean verifyPageColor(){
		WebElement webElemColor = commonfunction.getWebElement(webBody);
		String color = commonfunction.getWebCssValue(webElemColor,"background-color");
		String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");
		int hexValue1=Integer.parseInt(hexValue[0]);
		hexValue[1] = hexValue[1].trim();
		int hexValue2=Integer.parseInt(hexValue[1]);
		hexValue[2] = hexValue[2].trim();
		int hexValue3=Integer.parseInt(hexValue[2]);
		String actualColor = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
		if(!expectedColor.equalsIgnoreCase(actualColor)){
			System.out.println("Colour is not as per the requirement");
			return false;
		}
		else{
			System.out.println("Colour is as per the requirement");
			return true;
		}
	}
	
	
	
}
